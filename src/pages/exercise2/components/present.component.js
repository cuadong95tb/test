import React, {Component} from 'react';
import Form from './form.component';

class Present extends Component{
    render(){
        return (
            <div className="present text-format">
                <h4 className="text-center font-weight-bold">LỜI GIẢI</h4>
                <div className="mb-4">
                    <h6>Thuật toán:</h6>
                    <div>
                        <code>{`sumCalculator = (n) => {`}</code>
                        <div className="pl-3">
                            <code>{`return (n * (n + 1)) / 2;`}</code>
                        </div>
                        <code>{`}`}</code>
                    </div>
                </div>
                <Form/>
            </div>
        );
    }
}
export default Present;