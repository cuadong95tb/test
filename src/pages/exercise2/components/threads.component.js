import React from 'react';

function Threads() {
  return (
    <div className="threads">
        <div className="text-format">
            <p><strong>Câu 2: </strong> Việt chương trình đệ quy tính tổng các số từ 1 với n, với n là một số cho trước.</p>
            <p className="pl-5">
                n=0 -&gt; 0 <br/>
                n=1 -&gt; 1 <br/>
                n=3 -&gt; 6 <br/>
                n=10 -&gt; 55 <br/>
            </p>
        </div>
    </div>
  );
}

export default Threads;