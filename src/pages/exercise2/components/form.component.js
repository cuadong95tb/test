import React, {Component} from 'react';


class Form extends Component{
    constructor (props){
        super(props);
        this.state = {
            total : 0,
        }
    }
    sumCalculator = (n) => {
        return (n * (n + 1)) / 2;
    }
    isNaturalNumber = (n) => {
        n = n.toString();
        var n1 = Math.abs(n),
            n2 = parseInt(n, 10);
        return !isNaN(n1) && n2 === n1 && n1.toString() === n;
    }
    onChange = (event) => {
        let n = event.target.value;
        if (n && this.isNaturalNumber(n)){
            n = parseFloat(n);
            let result = this.sumCalculator(n);
            this.setState({
                total : result
            })
        }else {
            this.setState({
                total : 0
            })
        }
    }
    render(){
        return (
            <div className="form">
                <input 
                type="number" 
                className="form-control mb-3 mr-3" 
                style={{
                    maxWidth : '200px',
                    display : 'inline-block',
                }}
                min={0}
                max={9999}
                onChange = {this.onChange}
                />
                <span>Kết quả: <strong>{this.state.total}</strong></span>
            </div>
        );
    }
}


export default Form;