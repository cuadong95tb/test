import React from 'react';
import Threads from './components/threads.component';
import Present from './components/present.component';

function Exercise2() {
  return (
    <section>
        <div className="container">
            <div className="section-content">
            <Threads/>
            <Present/>
            </div>
        </div>
    </section>
  );
}

export default Exercise2;