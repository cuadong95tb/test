import React, {Component} from 'react';
import './popup.component.scss';

class Popup extends Component{
    hidePopup = (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.props.onClosePopup(false);
    }
    render(){
        return (
            <div className="popup" onClick={this.hidePopup}>
                <div className="popup__content" onClick={(event) => event.stopPropagation()}>
                    <iframe src="https://www.topcv.vn/xem-cv/61e99d2222ccdd6c1abfb08b16acd09f"></iframe>
                </div>
                <button className="popup__close" onClick={this.hidePopup}>x</button>
            </div>
        );
    }
}
export default Popup;