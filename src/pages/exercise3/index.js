import React, {Component} from 'react';
import Threads from './components/threads.component';
import Popup from './components/popup.component';

class Exercise3 extends Component {
  constructor(props){
    super(props);
    this.state = {
      showPopup : false
    }
  }
  showPopup = (event) => {
    event.preventDefault();
    this.setState({
      showPopup : true
    });
  }
  onClosePopup = (param) => {
    this.setState({
      showPopup : param
    });
  }
  render(){
    return (
      <section>
          <div className="container">
              <div className="section-content">
                <Threads/>
                <div className="present text-format">
                  <h4 className="text-center font-weight-bold">LỜI GIẢI</h4>
                  <div className="mb-4">
                      <button className="btn btn-primary" onClick={this.showPopup}>Click để hiển thị popup</button>
                  </div>
                </div>
              </div>
          </div>
          {this.state.showPopup ? <Popup onClosePopup={this.onClosePopup}/> : ''}
      </section>
    );
  }
}

export default Exercise3;