import React from 'react';

function CodeEx1() {
  return (
    <div className="pl-3 mb-3">
        <h6>Thuật toán:</h6>
        <code>{`ascending = (array) => {`}</code><br/>
        <div className="pl-3">
            <code>{`let arrSort = array;`}</code><br/>
            <code>{`for(let i = 0; i < arrSort.length - 1; i++){`}</code><br/>
            <div className="pl-3">
                <code>{`for(let j = i + 1; j < arrSort.length; j++){`}</code><br/>
                <div className="pl-3">
                    <code>{`if(arrSort[i] > arrSort[j]){`}</code><br/>
                    <div className="pl-3">
                        <code>{`let swap = arrSort[i];`}</code><br/>
                        <code>{`arrSort[i] = arrSort[j];`}</code><br/>
                        <code>{`arrSort[j] = swap;`}</code>
                    </div>
                    <code>{`}`}</code>
                </div>
                <code>{`}`}</code>
            </div>
            <code>{`}`}</code><br/>
            <code>{`return arrSort;`}</code>
        </div>
        <code>{`}`}</code>
    </div>
  );
}

export default CodeEx1;