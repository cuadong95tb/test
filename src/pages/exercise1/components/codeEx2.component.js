import React from 'react';

function CodeEx2() {
  return (
    <div className="pl-3 mb-3">
        <h6>Thuật toán:</h6>
        <code>{`ascendingAbsolute = (arr) => {`}</code><br/>
        <div className="pl-3">
            <code>{`let arrSort = arr;`}</code><br/>
            <code>{`for(let i = 0; i < arrSort.length - 1; i++){`}</code><br/>
            <div className="pl-3">
                <code>{`for(let j = i + 1; j < arrSort.length; j++){`}</code><br/>
                <div className="pl-3">
                    <code>{`let a = this.absoluteValue(arr[i]);`}</code><br/>
                    <code>{`let b = this.absoluteValue(arrSort[j])`}</code><br/>
                    <code>{`if((a > b) || (a === b && arr[i] > arrSort[j])){`}</code><br/>
                    <div className="pl-3">
                        <code>{`let swap = arrSort[i];`}</code><br/>
                        <code>{`arrSort[i] = arrSort[j];`}</code><br/>
                        <code>{`arrSort[j] = swap; `}</code><br/>
                    </div>
                    <code>{`}`}</code>
                </div>
                <code>{`}`}</code>
            </div>
            <code>{`}`}</code><br/>
            <code>{`return arrSort;`}</code>
        </div>
        <code>{`}`}</code>
    </div>
  );
}

export default CodeEx2;