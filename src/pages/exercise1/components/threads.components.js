import React from 'react';

function Threads() {
  return (
    <div className="threads">
        <div className="text-format">
            <p><strong>Câu 1: </strong> Cho một dãy số [1, 25, 7, -7, -3, 12, -22, 0]</p>
            <ol>
                <li>Sử dụng JavaScript để sắp xếp dãy số theo thứ tự tăng dần.</li>
                <li>Sắp xếp dãy số tăng dần theo giá trị tuyệt đối (trong trường hợp số 2 số bằng nhau thì số
                    âm được ưu tiên đứng trước)</li>
                <li>Sắp xếp tất cả các số dương trước, tất cả các số âm sau, hiển thị theo chiều tăng dần</li>
            </ol>
        </div>
    </div>
  );
}

export default Threads;