import React, {Component} from 'react';
import CodeEx1 from './codeEx1.component';
import CodeEx2 from './codeEx2.component';
import CodeEx3 from './codeEx3.component';


class Present extends Component{
    ascending = (array) => {
        let arrSort = array;
        for(let i = 0; i < arrSort.length - 1; i++){
            for(let j = i + 1; j < arrSort.length; j++){
                if(arrSort[i] > arrSort[j]){
                    let swap = arrSort[i];
                    arrSort[i] = arrSort[j];
                    arrSort[j] = swap;        
                }
            }   
        }
        return arrSort;
    }
    sortUpAscending = (event) => {
        event.preventDefault();
        let arrSort = [1, 25, 7, -7, -3, 12, -22, 0];
        let result = this.ascending(arrSort);
        this.props.onReceiveArrayEx1(result);
    }

    absoluteValue = (value) => {
        if (Number.isFinite(value)){
            return value < 0 ? (value * (-1)) : value;
        }
    }
    ascendingAbsolute = (arr) => {
        let arrSort = arr;
        for(let i = 0; i < arrSort.length - 1; i++){
            for(let j = i + 1; j < arrSort.length; j++){
                let a = this.absoluteValue(arr[i]);
                let b = this.absoluteValue(arrSort[j])
                if((a > b) || (a === b && arr[i] > arrSort[j])){
                    let swap = arrSort[i];
                    arrSort[i] = arrSort[j];
                    arrSort[j] = swap;        
                }
            }
        }
        return arrSort;
    }
    sortUpAbsolute = (event) => {
        event.preventDefault();
        let arrSort = [1, 25, 7, -7, -3, 12, -22, 0];
        let result = this.ascendingAbsolute(arrSort);
        this.props.onReceiveArrayEx2(result);
    }

    sortUpSlassifyYinAndYang = (array) => {
        let arrYin = [];
        let arrYang = [];
        for (let i = 0; i < array.length; i++){
            if (array[i] < 0) {
                arrYin.push(array[i])
            } else {
                arrYang.push(array[i])
            }
        }
        arrYin = this.ascending(arrYin);
        arrYang = this.ascending(arrYang);
        let result = arrYang.concat(arrYin);
        return result;
    }

    sortUp3 = (event) => {
        event.preventDefault();
        let arrSort = [1, 25, 7, -7, -3, 12, -22, 0];
        let result = this.sortUpSlassifyYinAndYang(arrSort);
        this.props.onReceiveArrayEx3(result);
    }
    render(){
        let {arrayEx1, arrayEx2, arrayEx3} = this.props;
        let arrResultEx1 = arrayEx1.map((item, index, items) => {
            return (
            <span key={index}>{item}{index === items.length - 1 ? '' : ', '}</span>
            )
        });
        let arrResultEx2 = arrayEx2.map((item, index, items) => {
            return (
            <span key={index}>{item}{index === items.length - 1 ? '' : ', '}</span>
            )
        });
        let arrResultEx3 = arrayEx3.map((item, index, items) => {
            return (
            <span key={index}>{item}{index === items.length - 1 ? '' : ', '}</span>
            )
        });
        return (
            <div className="present text-format">
                <h4 className="text-center font-weight-bold">LỜI GIẢI</h4>
                <p>
                    <strong>1. </strong>Sử dụng JavaScript để sắp xếp dãy số theo thứ tự tăng dần.
                </p>
                <CodeEx1/>
                <div className="pl-3">
                    <button className="btn btn-primary" onClick={this.sortUpAscending}>Hiện kết quả</button>
                    {arrResultEx1.length ? <span className="ml-4">[{arrResultEx1}]</span> : ''}
                </div>
                <p className="mt-4">
                    <strong>2. </strong>Sắp xếp dãy số tăng dần theo giá trị tuyệt đối (trong trường hợp số 2 số bằng nhau thì số âm được ưu tiên đứng trước)
                </p>
                <CodeEx2/>
                <div className="pl-3">
                    <button className="btn btn-primary" onClick={this.sortUpAbsolute}>Hiện kết quả</button>
                    {arrResultEx2.length ? <span className="ml-4">[{arrResultEx2}]</span> : ''}
                </div>
                <p className="mt-4">
                    <strong>3. </strong>Sắp xếp tất cả các số dương trước, tất cả các số âm sau, hiển thị theo chiều tăng dần
                </p>
                <CodeEx3/>
                <div className="pl-3">
                    <button className="btn btn-primary" onClick={this.sortUp3}>Hiện kết quả</button>
                    {arrResultEx3.length ? <span className="ml-4">[{arrResultEx3}]</span> : ''}
                </div>
            </div>
        );
    }
}


export default Present;