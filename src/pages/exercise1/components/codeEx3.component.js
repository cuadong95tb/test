import React from 'react';

function CodeEx3() {
  return (
    <div className="pl-3 mb-3">
        <h6>Thuật toán:</h6>
        <code>{`sortUpSlassifyYinAndYang = (array) => {`}</code><br/>
        <div className="pl-3">
            <code>{`let arrYin = [];`}</code><br/>
            <code>{`let arrYang = [];`}</code><br/>
            <code>{`for (let i = 0; i < array.length; i++){`}</code><br/>
            <div className="pl-3">
                <code>{`if (array[i] < 0) {`}</code><br/>
                <div className="pl-3">
                    <code>{`if(arrSort[i] > arrSort[j]){`}</code><br/>
                    <div className="pl-3">
                        <code>{`arrYin.push(array[i]);`}</code>
                    </div>
                    <code>{`} else {`}</code>
                    <div className="pl-3">
                        <code>{`arrYang.push(array[i]);`}</code>
                    </div>
                    <code>{`}`}</code>
                </div>
                <code>{`}`}</code>
            </div>
            <code>{`}`}</code><br/>
            <code>{`arrYin = this.ascending(arrYin);`}</code><br/>
            <code>{`arrYang = this.ascending(arrYang);`}</code><br/>
            <code>{`let result = arrYang.concat(arrYin);`}</code><br/>
            <code>{`return result;`}</code>
        </div>
        <code>{`}`}</code>
    </div>
  );
}

export default CodeEx3;
