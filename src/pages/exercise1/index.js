import React, {Component} from 'react';
import Threads from './components/threads.components';
import Present from './components/present.component';

class Exercise1 extends Component{
    constructor(props){
        super(props);
        this.state = {
            arrDefault : [1, 25, 7, -7, -3, 12, -22, 0],
            arrEx1 : [],
            arrEx2 : [],
            arrEx3 : [],
        }
    }
    onReceiveArrayEx1 = (param) => {
        this.setState({
            arrEx1 : param
        })
    }
    onReceiveArrayEx2 = (param) => {
        this.setState({
            arrEx2 : param
        })
    }
    onReceiveArrayEx3 = (param) => {
        this.setState({
            arrEx3 : param
        })
    }
    render(){
        return (
            <section>
                <div className="container">
                    <div className="section-content">
                        <Threads/>
                        <Present
                            arrayDefault = {this.state.arrDefault}
                            arrayEx1 = {this.state.arrEx1}
                            arrayEx2 = {this.state.arrEx2}
                            arrayEx3 = {this.state.arrEx3}
                            onReceiveArrayEx1 = {this.onReceiveArrayEx1}
                            onReceiveArrayEx2 = {this.onReceiveArrayEx2}
                            onReceiveArrayEx3 = {this.onReceiveArrayEx3}
                        />
                    </div>
                </div>
            </section>
        );
    }
}

export default Exercise1;