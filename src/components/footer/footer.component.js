import React from 'react';
import './footer.component.scss';

function Footer() {
  return (
    <footer className="footer">
        <div className="container text-center">
            This test was taken by <a href="https://www.topcv.vn/xem-cv/61e99d2222ccdd6c1abfb08b16acd09f" target="_blank"><strong className="d-inline-block">Đào Mai Lê</strong></a>
        </div>
    </footer>
  );
}

export default Footer;