import React from 'react';
import {NavLink} from "react-router-dom";
import './header.component.scss';

function Header() {
  return (
    <header className="header">
        <div className="container">
            <ul className="header__nav">
                <li>
                <NavLink to="/exercise-1" activeClassName="active">Bài 1</NavLink>
                </li>
                <li>
                <NavLink to="/exercise-2" activeClassName="active">Bài 2</NavLink>
                </li>
                <li>
                <NavLink to="/exercise-3" activeClassName="active">Bài 3</NavLink>
                </li>
            </ul>
        </div>
    </header>
  );
}

export default Header;