import React from 'react';
import './App.scss';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Header from './components/header/header.component';
import Footer from './components/footer/footer.component';
import Exercise1 from './pages/exercise1';
import Exercise2 from './pages/exercise2';
import Exercise3 from './pages/exercise3';

function App() {
  return (
    <Router>
        <Header/>
        <main className="main">
          <Switch>
            <Route exact path="/">
              <Exercise1/>
            </Route>
            <Route path="/exercise-1">
              <Exercise1/>
            </Route>
            <Route path="/exercise-2">
              <Exercise2/>
            </Route>
            <Route path="/exercise-3">
              <Exercise3/>
            </Route>
          </Switch>
        </main>
        <Footer/>
    </Router>
  );
}

export default App;
